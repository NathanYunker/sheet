#!/home/nathan/dev/projects/sheet/venv/bin/python
from django.core import management

if __name__ == "__main__":
    management.execute_from_command_line()
